import 'package:call_the_roll/base/debug_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DocumentsRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    debugUtil.mark();
    return DocumentsPage();
  }
}

class DocumentsPage extends StatefulWidget {
  DocumentsPage({Key key}) : super(key: key);

  @override
  _DocumentsPageState createState() => _DocumentsPageState();
}

class _DocumentsPageState extends State<DocumentsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Documents",
          style: Theme
              .of(context)
              .textTheme
              .subtitle2
              .copyWith(
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      body: Center(
        child: Text(
          "Documents",
          style: Theme
              .of(context)
              .textTheme
              .headline3,
        ),
      ),
    );
  }
}
