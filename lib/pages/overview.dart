import 'package:call_the_roll/base/debug_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class OverviewRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    debugUtil.mark();
    return OverviewPage();
  }
}

class OverviewPage extends StatefulWidget {
  OverviewPage({Key key}) : super(key: key);

  @override
  _OverviewPageState createState() => _OverviewPageState();
}

class _OverviewPageState extends State<OverviewPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Overview",
          style: Theme
              .of(context)
              .textTheme
              .subtitle2
              .copyWith(
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      body: Center(
        child: Text(
          "Overview",
          style: Theme
              .of(context)
              .textTheme
              .headline3,
        ),
      ),
    );
  }
}
