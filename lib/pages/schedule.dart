import 'package:call_the_roll/base/debug_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ScheduleRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    debugUtil.mark();
    return SchedulePage();
  }
}

class SchedulePage extends StatefulWidget {
  SchedulePage({Key key}) : super(key: key);

  @override
  _SchedulePageState createState() => _SchedulePageState();
}

class _SchedulePageState extends State<SchedulePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Schedule",
          style: Theme
              .of(context)
              .textTheme
              .subtitle2
              .copyWith(
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      body: Center(
        child: Text(
          "Schedule",
          style: Theme
              .of(context)
              .textTheme
              .headline3,
        ),
      ),
    );
  }
}
