import 'package:call_the_roll/base/debug_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class NotesRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    debugUtil.mark();
    return NotesPage();
  }
}

class NotesPage extends StatefulWidget {
  NotesPage({Key key}) : super(key: key);

  @override
  _NotesPageState createState() => _NotesPageState();
}

class _NotesPageState extends State<NotesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Notes",
          style: Theme
              .of(context)
              .textTheme
              .subtitle2
              .copyWith(
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      body: Center(
        child: Text(
          "Notes",
          style: Theme
              .of(context)
              .textTheme
              .headline3,
        ),
      ),
    );
  }
}
