import 'dart:io';
import 'dart:ui';

import 'package:call_the_roll/base/bloc.dart';
import 'package:call_the_roll/base/debug_util.dart';
import 'package:call_the_roll/blocs/students_bloc.dart';
import 'package:call_the_roll/model/student.dart';
import 'package:call_the_roll/model/student_list_divider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../application_model.dart';

class StudentsRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    debugUtil.mark();
    return StudentsPage();
  }
}

class StudentsPage extends StatefulWidget {
  StudentsPage({Key key}) : super(key: key);

  @override
  _StudentsPageState createState() => _StudentsPageState();
}

class _StudentsPageState extends State<StudentsPage> with TickerProviderStateMixin, WidgetsBindingObserver {
  StudentsBLoC studentsBLoC;
  final _messageController = TextEditingController();
  bool forceBuild;
  final _listKey = GlobalKey<AnimatedListState>();

  AnimationController _removeAnimationController;
  Animation<Offset> _offsetAnimationOutToDown;
  Animation<Offset> _offsetAnimationOutToUp;
  Animation<double> _doubleAnimation1To0;

  AnimationController _insertAnimationController;
  Animation<Offset> _offsetAnimationInFromUp;
  Animation<Offset> _offsetAnimationInFromDown;
  Animation<double> _doubleAnimation0To1;

  final SvgPicture checked = SvgPicture.asset("assets/images/ic_checkbox_fill.svg");
  final SvgPicture unchecked = SvgPicture.asset("assets/images/ic_decline_fill.svg");

  @override
  void initState() {
    super.initState();
    debugUtil.mark();
    WidgetsBinding.instance.addObserver(this);
    forceBuild = true;
    _removeAnimationController = AnimationController(duration: const Duration(milliseconds: 250), vsync: this);
    _insertAnimationController = AnimationController(duration: const Duration(milliseconds: 250), vsync: this);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    studentsBLoC = findBLoC<StudentsBLoC>(context);
  }

  @override
  void dispose() {
    _messageController.dispose();
    _removeAnimationController.dispose();
    _insertAnimationController.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Future<bool> didPopRoute() {
    bool override = false;
    if (studentsBLoC.currentState is StudentsShowMessageTemplate) {
      studentsBLoC.onBackClicked();
      override = true;
    }
    return new Future<bool>.value(override);
  }

  @override
  Widget build(BuildContext context) {
    return _scaffoldConsumer();
  }

  Widget _scaffoldConsumer() {
    return BLoCConsumer<StudentsBLoC, StudentsState>(
      tag: "_scaffoldConsumer",
      listener: (context, bloc, state) {
        if (state is StudentsInitial) {
          bloc.load();
        } else if (state is StudentsAskIfSend) {
          _askIfSendNotification(state.message).then((value) {
            if (value == "No") {
              bloc.onNoClicked();
            } else if (value == "Yes") {
              bloc.onYesClicked();
            }
          });
        } else if (state is StudentsSending) {
          _showSending();
        } else if (state is StudentsRollCallingDone) {
          _showRollCallingDone();
        } else if (state is StudentsSendSuccess) {
          _showSendingDone();
        }
      },
      buildWhen: (context, bloc, state) {
        return forceBuild || state is StudentsInitial || state is StudentsLoaded || state is StudentsShowMessageTemplate;
      },
      builder: (context, bloc, state) {
        return _scaffold(bloc, state);
      },
    );
  }

  Widget _scaffold(StudentsBLoC bloc, StudentsState state) {
    return Scaffold(
      appBar: _appBar(bloc, state),
      body: _scaffoldBodyConsumer(),
    );
  }

  Widget _appBar(StudentsBLoC bloc, StudentsState state) {
    if (state is StudentsInitial) {
      return AppBar(
        title: _appBarTitle(),
      );
    } else if (state is StudentsShowMessageTemplate) {
      return AppBar(
        title: _appBarTitle(),
        leading: _appBarLeading(bloc),
        actions: <Widget>[
          _sendButton(bloc),
        ],
      );
    } else {
      return AppBar(
        title: _appBarTitle(),
        actions: <Widget>[
          _doneButton(bloc),
        ],
      );
    }
  }

  Widget _appBarTitle() {
    return Text(
      "Attendance",
      style: Theme
          .of(context)
          .textTheme
          .subtitle2
          .copyWith(
        fontWeight: FontWeight.w600,
      ),
    );
  }

  Widget _scaffoldBodyConsumer() {
    return BLoCConsumer<StudentsBLoC, StudentsState>(
      tag: "_scaffoldBodyConsumer",
      buildWhen: (context, bloc, state) {
        return forceBuild || state is StudentsInitial || state is StudentsLoaded || state is StudentsShowMessageTemplate;
      },
      builder: (context, bloc, state) {
        if (state is StudentsShowMessageTemplate) {
          return _messageTemplate(state.message);
        } else {
          return _callTheRoll();
        }
      },
    );
  }

  Widget _callTheRoll() {
    return Column(
      children: <Widget>[
        _counterConsumer(),
        SizedBox(
          height: 16,
        ),
        _studentListConsumer(),
        SizedBox(
          height: 16,
        ),
      ],
    );
  }

  Widget _appBarLeading(StudentsBLoC bloc) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      color: ApplicationModel.COLOR_PRIMARY,
      onPressed: () => bloc.onBackClicked(),
    );
  }

  Widget _counterConsumer() {
    return BLoCConsumer<StudentsBLoC, StudentsState>(
      tag: "_counterConsumer",
      buildWhen: (context, bloc, state) {
        return state is StudentsInitial || state is StudentsLoaded || state is StudentsSwitchAttendanceDone;
      },
      builder: (context, bloc, state) {
        if (state is StudentsLoaded || state is StudentsSwitchAttendanceDone) {
          return _counter(state);
        }
        return null;
      },
    );
  }

  void _createRemoveAnimations(Animation<double> parentAnimation, bool reset) {
    _offsetAnimationOutToDown = Tween<Offset>(begin: Offset.zero, end: Offset(0.0, 2.0)).animate(
      CurvedAnimation(parent: parentAnimation, curve: Curves.linear),
    );
    _offsetAnimationOutToUp = Tween<Offset>(begin: Offset.zero, end: Offset(0.0, -2.0)).animate(
      CurvedAnimation(parent: parentAnimation, curve: Curves.linear),
    );
    _doubleAnimation1To0 = Tween<double>(begin: 1.0, end: 0.0).animate(
      CurvedAnimation(parent: parentAnimation, curve: Curves.linear),
    );
    if (reset && parentAnimation is AnimationController) {
      parentAnimation.reset();
      parentAnimation.forward();
    }
  }

  void _createInsertAnimations(Animation<double> parentAnimation, bool reset) {
    _offsetAnimationInFromUp = Tween<Offset>(begin: Offset(0.0, -2.0), end: Offset.zero).animate(
      CurvedAnimation(parent: parentAnimation, curve: Curves.linear),
    );
    _offsetAnimationInFromDown = Tween<Offset>(begin: Offset(0.0, 2.0), end: Offset.zero).animate(
      CurvedAnimation(parent: parentAnimation, curve: Curves.linear),
    );
    _doubleAnimation0To1 = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(parent: parentAnimation, curve: Curves.linear),
    );
    if (reset && parentAnimation is AnimationController) {
      parentAnimation.reset();
      parentAnimation.forward();
    }
  }

  Widget _studentListConsumer() {
    return BLoCConsumer<StudentsBLoC, StudentsState>(
      tag: "_studentListConsumer",
      notifyListenerCurrentStateWhenInitiate: false,
      listener: (context, bloc, state) {
        if (ApplicationModel.ANIMATED_LIST && state is StudentsSwitchAttendanceDone) {
          debugUtil.log("beforeIndex: ${state.beforeIndex}");
          debugUtil.log("afterIndex: ${state.afterIndex}");

          _listKey.currentState.removeItem(
            state.beforeIndex,
                (context, animation) {
              //_createRemoveAnimations(animation, false);

              return _studentListRemovedItemSizeDownFadeOut(context, state.student, animation);

//              Animation<Offset> offsetAnimation = state.afterIndex > state.beforeIndex ? _offsetAnimationOutToDown : _offsetAnimationOutToUp;
//              return _studentListRemovedItemSizeDownFadeOutSlide(context, state.student, offsetAnimation, _doubleAnimation1To0);
            },
//            duration: Duration(seconds: 8),
          );
          if (state.beforeIndex != state.afterIndex) {
            _listKey.currentState.insertItem(
              state.afterIndex,
//              duration: Duration(seconds: 8),
            );
          }
        }
      },
      buildWhen: (context, bloc, state) {
        return forceBuild || state is StudentsInitial || state is StudentsLoading || state is StudentsLoaded || (!ApplicationModel.ANIMATED_LIST && state is StudentsSwitchAttendanceDone);
      },
      builder: (context, bloc, state) {
        forceBuild = false;
        if (state is StudentsInitial || state is StudentsLoading) {
          return _progressIndicator();
        } else {
          return _studentList();
        }
      },
    );
  }

  Widget _doneButton(StudentsBLoC bloc) {
    return FlatButton(
      child: Text(
        "Done",
        style: Theme
            .of(context)
            .textTheme
            .subtitle1
            .copyWith(
          color: ApplicationModel.COLOR_PRIMARY,
          fontWeight: FontWeight.w600,
        ),
      ),
      onPressed: () => bloc.onDoneClicked(),
    );
  }

  Widget _sendButton(StudentsBLoC bloc) {
    return FlatButton(
      child: Text(
        "Send",
        style: Theme
            .of(context)
            .textTheme
            .subtitle1
            .copyWith(
          color: ApplicationModel.COLOR_PRIMARY,
          fontWeight: FontWeight.w600,
        ),
      ),
      onPressed: () => bloc.onSendClicked(_messageController.text),
    );
  }

  Widget _counter(StudentsState state) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          constraints: BoxConstraints(minHeight: 24, minWidth: 60),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12.0),
            color: ApplicationModel.ACCENT_GREEN,
          ),
          child: Text(
            state.counterString,
            style: Theme
                .of(context)
                .textTheme
                .subtitle1
                .copyWith(
              fontFamily: "Roboto",
              fontWeight: FontWeight.w500,
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }

  Widget _studentList() {
    return Expanded(
      child: AnimatedList(
        key: _listKey,
//        padding: const EdgeInsets.only(left: 12, top: 6, right: 12, bottom: 6),
        initialItemCount: studentsBLoC.currentState.studentList.length,
        itemBuilder: (_, index, animation) {
          //_createInsertAnimations(_insertAnimationController, true);
          Student student = studentsBLoC.currentState.studentList[index];
          if (student is StudentListDivider) {
            return _divider();
          } else {
            return _studentListItemSizeUpFadeIn(context, studentsBLoC, student, animation);
          }
        },
      ),
    );
  }

  Widget _progressIndicator() {
    return Expanded(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget _divider() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 20.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              height: 1.0,
              decoration: BoxDecoration(
                color: ApplicationModel.DIVIDER,
              ),
            ),
          ),
          SizedBox(
            width: 16,
          ),
          Text(
            "HERE",
            style: Theme
                .of(context)
                .textTheme
                .caption
                .copyWith(
              fontWeight: FontWeight.w600,
              color: ApplicationModel.COLOR_CAPTION,
            ),
          ),
          SizedBox(
            width: 16,
          ),
          Expanded(
            child: Container(
              height: 1.0,
              decoration: BoxDecoration(
                color: ApplicationModel.DIVIDER,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _studentListItemSizeUpFadeIn(BuildContext context, StudentsBLoC studentsBLoC, Student student, Animation<double> animation) {
    return FadeTransition(
      opacity: animation,
      child: SizeTransition(
        axis: Axis.vertical,
        sizeFactor: animation,
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 16.0),
          decoration: new BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20.0),
            boxShadow: [
              BoxShadow(
                color: ApplicationModel.SHADOW,
                offset: Offset(2.0, 1.0),
                blurRadius: 10.0,
                spreadRadius: 0.0,
              )
            ],
          ),
          child: Card(
            margin: EdgeInsets.all(0.0),
            elevation: 0.0,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
            clipBehavior: Clip.antiAlias,
            child: InkWell(
              onTap: () {
                studentsBLoC.switchAttendance(student);
              },
              child: Padding(
                padding: const EdgeInsets.only(left: 16.0, top: 16.0, right: 20.0, bottom: 16.0),
                child: Row(
                  children: <Widget>[
                    _avatarOrInitials(student),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: Text(
                        student.name,
                        style: Theme
                            .of(context)
                            .textTheme
                            .bodyText2
                            .copyWith(
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
//                  Checkbox(
//                    value: student.attendance,
//                    onChanged: (bool value) {
//                      studentsBLoC.switchAttendance(student);
//                    },
//                  ),
                    Container(
                      child: student.attendance ? checked : unchecked,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _studentListItemSizeUpFadeInSlide(BuildContext context, StudentsBLoC studentsBLoC, Student student, Animation<Offset> slideAnimation, Animation<double> fadeInAnimation) {
    return SlideTransition(
      position: slideAnimation,
      child: FadeTransition(
        opacity: fadeInAnimation,
        child: SizeTransition(
          axis: Axis.vertical,
          sizeFactor: fadeInAnimation,
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 16.0),
            decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20.0),
              boxShadow: [
                BoxShadow(
                  color: ApplicationModel.SHADOW,
                  offset: Offset(2.0, 1.0),
                  blurRadius: 10.0,
                  spreadRadius: 0.0,
                )
              ],
            ),
            child: Card(
              margin: EdgeInsets.all(0.0),
              elevation: 0.0,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
              clipBehavior: Clip.antiAlias,
              child: InkWell(
                onTap: () {
                  studentsBLoC.switchAttendance(student);
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 16.0, top: 16.0, right: 20.0, bottom: 16.0),
                  child: Row(
                    children: <Widget>[
                      _avatarOrInitials(student),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Text(
                          student.name,
                          style: Theme
                              .of(context)
                              .textTheme
                              .bodyText2
                              .copyWith(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
//                    Checkbox(
//                      value: student.attendance,
//                      onChanged: (bool value) {
//                        studentsBLoC.switchAttendance(student);
//                      },
//                    ),
                      Container(
                        child: student.attendance ? checked : unchecked,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _studentListRemovedItemSizeDownFadeOut(BuildContext context, Student student, Animation<double> animation) {
    return FadeTransition(
      opacity: animation,
      child: SizeTransition(
        axis: Axis.vertical,
        sizeFactor: animation,
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 16.0),
          decoration: new BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20.0),
            boxShadow: [
              BoxShadow(
                color: ApplicationModel.SHADOW,
                offset: Offset(2.0, 1.0),
                blurRadius: 10.0,
                spreadRadius: 0.0,
              )
            ],
          ),
          child: Card(
            margin: EdgeInsets.all(0.0),
            elevation: 0.0,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
            clipBehavior: Clip.antiAlias,
            child: Padding(
              padding: const EdgeInsets.only(left: 16.0, top: 16.0, right: 20.0, bottom: 16.0),
              child: Row(
                children: <Widget>[
                  _avatarOrInitials(student),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: Text(
                      student.name,
                      style: Theme
                          .of(context)
                          .textTheme
                          .bodyText2
                          .copyWith(
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
//                Checkbox(
//                  value: student.attendance,
//                  onChanged: (bool value) {},
//                ),
                  Container(
                    child: student.attendance ? checked : unchecked,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _studentListRemovedItemSizeDownFadeOutSlide(BuildContext context, Student student, Animation<Offset> slideAnimation, Animation<double> fadeOutAnimation) {
    return SlideTransition(
      position: slideAnimation,
      child: FadeTransition(
        opacity: fadeOutAnimation,
        child: SizeTransition(
          axis: Axis.vertical,
          sizeFactor: fadeOutAnimation,
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 16.0),
            decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20.0),
              boxShadow: [
                BoxShadow(
                  color: ApplicationModel.SHADOW,
                  offset: Offset(2.0, 1.0),
                  blurRadius: 10.0,
                  spreadRadius: 0.0,
                )
              ],
            ),
            child: Card(
              margin: EdgeInsets.all(0.0),
              elevation: 0.0,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
              clipBehavior: Clip.antiAlias,
              child: Padding(
                padding: const EdgeInsets.only(left: 16.0, top: 16.0, right: 20.0, bottom: 16.0),
                child: Row(
                  children: <Widget>[
                    _avatarOrInitials(student),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: Text(
                        student.name,
                        style: Theme
                            .of(context)
                            .textTheme
                            .bodyText2
                            .copyWith(
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
//                  Checkbox(
//                    value: student.attendance,
//                    onChanged: (bool value) {},
//                  ),
                    Container(
                      child: student.attendance ? checked : unchecked,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _avatarOrInitials(Student student) {
    if (student.avatar != null) {
      return Container(
        width: 40.0,
        height: 40.0,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: ApplicationModel.AVATAR_BACKGROUND,
          image: DecorationImage(
            fit: BoxFit.contain,
            image: AssetImage(student.avatar),
          ),
        ),
      );
    } else {
      return Container(
        width: 40.0,
        height: 40.0,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: student.color,
        ),
        child: Text(
          student.initials,
          style: Theme
              .of(context)
              .textTheme
              .subtitle2
              .copyWith(
            color: Colors.white,
          ),
        ),
      );
    }
  }

  Widget _messageTemplate(String message) {
    _messageController.text = message;
    return Container(
      margin: EdgeInsets.all(16.0),
      decoration: new BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20.0),
        boxShadow: [
          BoxShadow(
            color: ApplicationModel.SHADOW,
            offset: Offset(2.0, 1.0),
            blurRadius: 10.0,
            spreadRadius: 0.0,
          )
        ],
      ),
      child: Card(
        elevation: 0.0,
        margin: const EdgeInsets.all(0.0),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        clipBehavior: Clip.antiAlias,
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 24,
              ),
              Text(
                "Your message:",
                style: Theme
                    .of(context)
                    .textTheme
                    .headline5
                    .copyWith(
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Expanded(
                child: TextFormField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                  ),
                  style: Theme
                      .of(context)
                      .textTheme
                      .bodyText1
                      .copyWith(
                    fontSize: 18,
                    fontFamily: "Roboto",
                    color: ApplicationModel.COLOR_CAPTION,
                  ),
                  controller: _messageController,
                  keyboardType: TextInputType.multiline,
                  maxLength: null,
                  maxLines: null,
                  enabled: true,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<String> _askIfSendNotification(String message) {
    if (Platform.isIOS) {
      return showCupertinoDialog(
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: () async => false,
            child: CupertinoAlertDialog(
              title: Text(
                'Student Absent',
                style: Theme
                    .of(context)
                    .textTheme
                    .headline6
                    .copyWith(fontFamily: "SFProText", fontSize: 17),
              ),
              content: Column(
                children: <Widget>[
                  SizedBox(
                    width: 1.0,
                    height: 12.0,
                  ),
                  Text(
                    message,
                    style: Theme
                        .of(context)
                        .textTheme
                        .bodyText2
                        .copyWith(fontFamily: "SFProText", fontSize: 13),
                  ),
                ],
              ),
              actions: <Widget>[
                CupertinoDialogAction(
                  child: Text(
                    'No',
                  ),
                  onPressed: () {
                    Navigator.of(context).pop("No");
                  },
                ),
                CupertinoDialogAction(
                  child: Text(
                    'Yes',
                  ),
                  onPressed: () {
                    Navigator.of(context).pop("Yes");
                  },
                ),
              ],
            ),
          );
        },
      );
    } else {
      return showDialog(
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: () async => false,
            child: AlertDialog(
              title: Text('Student Absent'),
              content: Text(message),
              actions: <Widget>[
                FlatButton(
                  child: Text('No'),
                  onPressed: () {
                    Navigator.of(context).pop("No");
                  },
                ),
                FlatButton(
                  child: Text('Yes'),
                  onPressed: () {
                    Navigator.of(context).pop("Yes");
                  },
                ),
              ],
            ),
          );
        },
        barrierDismissible: false,
      );
    }
  }

  Future<String> _showSending() {
    if (Platform.isIOS) {
      return showCupertinoDialog(
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: () async => false,
            child: CupertinoAlertDialog(
              title: Text(
                'Sending notification...',
                style: Theme
                    .of(context)
                    .textTheme
                    .headline6
                    .copyWith(fontFamily: "SFProText", fontSize: 17),
              ),
              content: Column(
                children: <Widget>[
                  SizedBox(
                    width: 1.0,
                    height: 12.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircularProgressIndicator(),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      );
    } else {
      return showDialog(
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: () async => false,
            child: AlertDialog(
              title: Text('Sending notification...'),
              content: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                ],
              ),
            ),
          );
        },
        barrierDismissible: false,
      );
    }
  }

  void _showSendingDone() {
    Navigator.of(context).pop("Done");
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text("Notification is sent successfully."),
    ));
  }

  void _showRollCallingDone() {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text("Roll calling is done."),
    ));
  }
}
