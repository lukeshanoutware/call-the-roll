import 'package:call_the_roll/application_model.dart';
import 'package:call_the_roll/base/bloc.dart';
import 'package:call_the_roll/base/debug_util.dart';
import 'package:call_the_roll/blocs/bottom_navigator_bloc.dart';
import 'package:call_the_roll/blocs/students_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class NavigationHubRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    debugUtil.mark();
    return NavigationHubPage();
  }
}

class NavigationHubPage extends StatefulWidget {
  NavigationHubPage({Key key}) : super(key: key);

  @override
  _NavigationHubPageState createState() => _NavigationHubPageState();
}

class _NavigationHubPageState extends State<NavigationHubPage> {
  final BottomNavigatorBLoC bottomNavigatorBLoC = BottomNavigatorBLoC();
  final StudentsBLoC studentsBLoC = StudentsBLoC();

  @override
  Widget build(BuildContext context) {
    debugUtil.mark();
    return MultiBLoCsProvider(
      bLoCs: [
        bottomNavigatorBLoC,
        studentsBLoC,
      ],
      child: BLoCConsumer<BottomNavigatorBLoC, BottomNavigatorState>(
        tag: "_NavigationHubPageState consumer",
        builder: (context, bloc, state) {
          return Scaffold(
            body: state.route,
            bottomNavigationBar: BottomNavigationBar(
              items: bloc
                  .getAllItems()
                  .map((item) => BottomNavigationBarItem(
                icon: item.icon,
                title: Text(item.title),
              ))
                  .toList(),
              selectedLabelStyle: Theme
                  .of(context)
                  .textTheme
                  .overline
                  .copyWith(
                fontFamily: "Roboto",
                fontSize: 10,
              ),
              unselectedLabelStyle: Theme
                  .of(context)
                  .textTheme
                  .overline
                  .copyWith(
                fontFamily: "Roboto",
                fontSize: 10,
              ),
              selectedItemColor: ApplicationModel.COLOR_PRIMARY,
              unselectedItemColor: ApplicationModel.COLOR_CAPTION,
              showUnselectedLabels: true,
              type: BottomNavigationBarType.fixed,
              currentIndex: state.index,
              backgroundColor: ApplicationModel.BOTTOM_NAVIGATION_BAR_BACKGROUND,
              onTap: (index) => bloc.navigateToIndex(index),
            ),
          );
        },
      ),
    );
  }
}
