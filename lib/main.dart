import 'package:call_the_roll/application_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'pages/navigation_hub.dart';

void main() {
  debugPaintSizeEnabled = false;
  runApp(CallTheRollApp());
}

class CallTheRollApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Call the Roll',
      theme: ThemeData(
        scaffoldBackgroundColor: ApplicationModel.WINDOW_BACKGROUND,
        fontFamily: "Poppins",
        primaryColor: ApplicationModel.COLOR_PRIMARY,
        textTheme: TextTheme(caption: TextStyle(color: ApplicationModel.COLOR_CAPTION)),
        primaryColorBrightness: Brightness.dark,
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          brightness: Brightness.light,
          color: ApplicationModel.WINDOW_BACKGROUND,
          elevation: 0,
        ),
      ),
      home: NavigationHubPage(),
    );
  }
}
