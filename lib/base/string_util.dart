import 'package:flutter/widgets.dart';

String getInitials(String nameString) {
  if (nameString == null || nameString.isEmpty) return "";
  List<String> nameArray = nameString.replaceAll(new RegExp(r"\s+\b|\b\s"), " ").split(" ");
  String initials = ((nameArray[0])[0] != null ? (nameArray[0])[0] : " ") + (nameArray.length == 1 ? "" : (nameArray[nameArray.length - 1])[0]);
  return initials;
}

Color getColor(String string, double saturation, double lightness) {
  double hue = (string.hashCode % 360).toDouble();
  HSLColor hslColor = HSLColor.fromAHSL(1.0, hue, saturation, lightness);
  return hslColor.toColor();
}
