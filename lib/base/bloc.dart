import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'debug_util.dart';

class BLoCTransaction<E, S> {
  final S currentState;
  final E event;
  final Object error;
  final S nextState;

  BLoCTransaction(this.currentState, this.event, this.error, this.nextState);

  @override
  String toString() {
    return """BLoCTransaction =>
    currentState: $currentState
    event       : $event
    error       : $error
    nextState   : $nextState
    <=""";
  }
}

abstract class BLoC<E, S> {
  final StreamController<E> _eventController = StreamController<E>();
  final StreamController<S> _stateController = StreamController<S>.broadcast();
  StreamSubscription<E> _eventSubscription;
  S _currentState;

  Stream<S> get stream => _stateController.stream;

  S get currentState => _currentState;

  S get initialState;

  BLoC() {
    _currentState = initialState;
    _eventSubscription = _eventController.stream.listen((data) => _dataHandler(data), onError: (error, stackTrace) => _errorHandler(error, stackTrace));
  }

  void _dataHandler(E event) {
    mapEventToStates(event).forEach((nextState) {
      _generateCurrentState(event, null, nextState);
    });
  }

  void _errorHandler(Object error, StackTrace stackTrace) {
    mapErrorToStates(error, stackTrace).forEach((errorState) {
      _generateCurrentState(null, error, errorState);
    });
  }

  void _generateCurrentState(E event, Object error, S newState) {
    if (newState != null && !_stateController.isClosed) {
      onTransaction(BLoCTransaction(_currentState, event, error, newState));
      _currentState = newState;
      _stateController.sink.add(_currentState);
    }
  }

  void addEvent(E event) {
    _eventController.sink.add(event);
  }

  void dispatchState(S state) {
    _generateCurrentState(null, null, state);
  }

  Stream<S> mapEventToStates(E event);

  Stream<S> mapErrorToStates(Object error, StackTrace stackTrace) => Stream<S>.empty();

  Future<void> onTransaction(BLoCTransaction<E, S> transaction) async {}

  @mustCallSuper
  Future<void> dispose() async {
    await _eventSubscription.cancel();
    await _eventController.close();
    await _stateController.close();
  }
}

class BLoCProvider<B extends BLoC> extends StatefulWidget {
  final B bLoC;
  final Widget child;

  BLoCProvider({Key key, @required this.bLoC, @required this.child}) : super(key: key);

  @override
  _BLoCProviderState createState() {
    debugUtil.mark();
    return _BLoCProviderState<B>();
  }

  static B of<B extends BLoC>(BuildContext context) {
    return _BLoCProviderInheritedWidget.of<B>(context);
  }
}

class _BLoCProviderState<B extends BLoC> extends State<BLoCProvider<B>> {
  @override
  void initState() {
    super.initState();
    debugUtil.mark();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    debugUtil.mark();
  }

  @override
  void didUpdateWidget(BLoCProvider<B> oldWidget) {
    super.didUpdateWidget(oldWidget);
    debugUtil.mark();
  }

  @override
  Widget build(BuildContext context) {
    debugUtil.mark();
    return _BLoCProviderInheritedWidget<B>(bLoC: widget.bLoC, child: widget.child);
  }

  @override
  void dispose() {
    debugUtil.mark();
    widget.bLoC.dispose();
    super.dispose();
  }
}

class _BLoCProviderInheritedWidget<B extends BLoC> extends InheritedWidget {
  final B bLoC;

  _BLoCProviderInheritedWidget({Key key, @required Widget child, @required this.bLoC}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_BLoCProviderInheritedWidget old) {
    return bLoC != old.bLoC;
  }

  static B of<B extends BLoC>(BuildContext context) {
    final _BLoCProviderInheritedWidget _bLoCProviderInheritedWidget = context.dependOnInheritedWidgetOfExactType<_BLoCProviderInheritedWidget<B>>();
    if (_bLoCProviderInheritedWidget == null) {
      throw Exception("BLoCProvider.of<$B>() called with a context that does not contain a BLoCProvider<$B>.");
    } else {
      return _bLoCProviderInheritedWidget.bLoC;
    }
  }
}

class MultiBLoCsProvider extends StatefulWidget {
  final List<BLoC> bLoCs;
  final Widget child;

  MultiBLoCsProvider({Key key, @required this.bLoCs, @required this.child}) : super(key: key);

  @override
  _MultiBLoCsProviderState createState() {
    debugUtil.mark();
    return _MultiBLoCsProviderState();
  }

  static B of<B extends BLoC>(BuildContext context) {
    return _MultiBLoCsProviderInheritedWidget.of<B>(context);
  }
}

class _MultiBLoCsProviderState extends State<MultiBLoCsProvider> {
  @override
  void initState() {
    super.initState();
    debugUtil.mark();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    debugUtil.mark();
  }

  @override
  void didUpdateWidget(MultiBLoCsProvider oldWidget) {
    super.didUpdateWidget(oldWidget);
    debugUtil.mark();
  }

  @override
  Widget build(BuildContext context) {
    debugUtil.mark();
    return _MultiBLoCsProviderInheritedWidget(bLoCs: widget.bLoCs, child: widget.child);
  }

  @override
  void dispose() {
    debugUtil.mark();
    widget.bLoCs.forEach((bloC) {
      bloC.dispose();
    });
    super.dispose();
  }
}

class _MultiBLoCsProviderInheritedWidget extends InheritedWidget {
  final List<BLoC> bLoCs;

  _MultiBLoCsProviderInheritedWidget({Key key, @required Widget child, @required this.bLoCs}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_MultiBLoCsProviderInheritedWidget old) {
    return bLoCs != old.bLoCs;
  }

  static B of<B extends BLoC>(BuildContext context) {
    final _MultiBLoCsProviderInheritedWidget _multiBLoCsProviderInheritedWidget = context.dependOnInheritedWidgetOfExactType<_MultiBLoCsProviderInheritedWidget>();
    if (_multiBLoCsProviderInheritedWidget == null) {
      throw Exception("MultiBLoCsProvider.of<$B>() called with a context that does not contain a MultiBLoCsProvider.");
    } else {
      return _multiBLoCsProviderInheritedWidget.bLoCs.firstWhere((bloC) {
        return (bloC is B);
      });
    }
  }
}

typedef BLoCStateListener<B extends BLoC, S> = void Function(BuildContext context, B bLoC, S state);
typedef BLoCStateBuilder<B extends BLoC, S> = Widget Function(BuildContext context, B bLoC, S state);
typedef BLoCStateBuildCondition<B extends BLoC, S> = bool Function(BuildContext context, B bLoC, S state);

class BLoCConsumer<B extends BLoC, S> extends StatefulWidget {
  static final Widget emptyWidgetFill = Container();
  static final Widget emptyWidgetShrink = Container(width: 0.0, height: 0.0);
  static final Widget funkyWidget = Icon(Icons.filter_vintage, color: Colors.red);
  final B bLoC;
  final BLoCStateListener<B, S> listener;
  final BLoCStateBuilder<B, S> builder;
  final BLoCStateBuildCondition<B, S> buildWhen;
  final Widget noBuilderWidget;
  final Widget noStateWidget;
  final Widget nullReplacementWidget;
  final bool notifyListenerCurrentStateWhenInitiate;
  final String tag;

  BLoCConsumer({
    Key key,
    this.bLoC,
    this.listener,
    this.builder,
    this.buildWhen,
    this.noBuilderWidget,
    this.noStateWidget,
    this.nullReplacementWidget,
    this.notifyListenerCurrentStateWhenInitiate = true,
    this.tag,
  }) : super(key: key);

  @override
  _BLoCConsumerState createState() {
    debugUtil.mark();
    return _BLoCConsumerState<B, S>();
  }
}

class _BLoCConsumerState<B extends BLoC, S> extends State<BLoCConsumer<B, S>> {
  B bLoC;
  StreamSubscription<S> _streamSubscription;

  @override
  void initState() {
    super.initState();
    debugUtil.mark(widget.tag);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    debugUtil.mark(widget.tag);
    bLoC = widget.bLoC ?? findBLoC<B>(context);
  }

  @override
  void didUpdateWidget(BLoCConsumer<B, S> oldWidget) {
    super.didUpdateWidget(oldWidget);
    debugUtil.mark(widget.tag);
  }

  @override
  Widget build(BuildContext context) {
    debugUtil.mark(widget.tag);
    _attachListener();
    return _buildContent(context);
  }

  @override
  void dispose() {
    debugUtil.mark(widget.tag);
    _detachListener();
    super.dispose();
  }

  Future<void> _attachListener() async {
    debugUtil.mark(widget.tag);
    if (widget.listener != null) {
      await _streamSubscription?.cancel();
      _streamSubscription = null;
      _streamSubscription = bLoC.stream.listen((data) {
        debugUtil.log("Calling ${widget.tag ?? this.runtimeType} listener for $data");
        widget.listener(context, bLoC, data);
      });
      if (widget.notifyListenerCurrentStateWhenInitiate) {
        debugUtil.log("Calling ${widget.tag ?? this.runtimeType} listener for ${bLoC.currentState}");
        widget.listener(context, bLoC, bLoC.currentState);
      }
    }
  }

  Future<void> _detachListener() async {
    debugUtil.mark(widget.tag);
    if (widget.listener != null) {
      await _streamSubscription?.cancel();
      _streamSubscription = null;
    }
  }

  bool _shouldBuild(S state) {
    return state != null && (widget.buildWhen == null || widget.buildWhen(context, bLoC, state));
  }

  Widget _buildContent(BuildContext context) {
    debugUtil.mark(widget.tag);
    if (widget.builder != null) {
      return StreamBuilder<S>(
        stream: bLoC.stream.where((state) => _shouldBuild(state)),
        initialData: _shouldBuild(bLoC.currentState) ? bLoC.currentState : null,
        builder: (BuildContext context, AsyncSnapshot<S> snapshot) {
          if (snapshot.hasData) {
            debugUtil.log("Calling ${widget.tag ?? this.runtimeType} builder for ${snapshot.data}");
            return widget.builder(context, bLoC, snapshot.data) ?? widget.nullReplacementWidget ?? BLoCConsumer.emptyWidgetShrink;
          } else {
            return widget.noStateWidget ?? BLoCConsumer.emptyWidgetShrink;
          }
        },
      );
    } else {
      return widget.noBuilderWidget;
    }
  }
}

class BLoCNotFoundException<B extends BLoC> implements Exception {
  String toString() => "BLoCNotFoundException: $B could not be found in the context.";
}

B findBLoC<B extends BLoC>(BuildContext context) {
  B bloC;
  try {
    bloC = BLoCProvider.of<B>(context);
  } catch (e) {
    debugUtil.log(e.toString());
    try {
      bloC = MultiBLoCsProvider.of<B>(context);
    } catch (e) {
      debugUtil.log(e.toString());
      throw BLoCNotFoundException<B>();
    }
  }
  return bloC;
}
