import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart';

import 'debug_util.dart';

class HttpStatus {
  final int code;
  final String _message;

  String get message => _message ?? _getMessage();

  const HttpStatus._(this.code, [this._message]);

  factory HttpStatus.fromCode(int code, String message) => HttpStatus._(code, message);

  factory HttpStatus.fromResponse(Response response) => HttpStatus._(response.statusCode, response.reasonPhrase);

  static const HttpStatus OK = HttpStatus._(200);
  static const HttpStatus CREATED = HttpStatus._(201);
  static const HttpStatus NON_AUTHORITATIVE_INFORMATION = HttpStatus._(203);
  static const HttpStatus NO_CONTENT = HttpStatus._(204);
  static const HttpStatus NOT_MODIFIED = HttpStatus._(304);
  static const HttpStatus BAD_REQUEST = HttpStatus._(400);
  static const HttpStatus UNAUTHORIZED = HttpStatus._(401);
  static const HttpStatus FORBIDDEN = HttpStatus._(403);
  static const HttpStatus NOT_FOUND = HttpStatus._(404);
  static const HttpStatus METHOD_NOT_ALLOWED = HttpStatus._(405);
  static const HttpStatus CONFLICT = HttpStatus._(409);
  static const HttpStatus GONE = HttpStatus._(410);
  static const HttpStatus UNSUPPORTED_MEDIA_TYPE = HttpStatus._(415);
  static const HttpStatus UNPROCESSABLE_ENTITY = HttpStatus._(422);
  static const HttpStatus TOO_MANY_REQUEST = HttpStatus._(429);
  static const HttpStatus INTERNAL_SERVER_ERROR = HttpStatus._(500);

  String _getMessage() {
    switch (this) {
      case OK:
        return "OK";
      case CREATED:
        return "Created";
      case NON_AUTHORITATIVE_INFORMATION:
        return "Non-Authoritative Information";
      case NO_CONTENT:
        return "No Content";
      case NOT_MODIFIED:
        return "Not Modified";
      case BAD_REQUEST:
        return "Bad Request";
      case UNAUTHORIZED:
        return "Unauthorized";
      case FORBIDDEN:
        return "Forbidden";
      case NOT_FOUND:
        return "Not Found";
      case METHOD_NOT_ALLOWED:
        return "Method Not Allowe";
      case CONFLICT:
        return "Conflict";
      case GONE:
        return "Gone";
      case UNSUPPORTED_MEDIA_TYPE:
        return "Unsupported Media Type";
      case UNPROCESSABLE_ENTITY:
        return "Unprocessable Entity";
      case TOO_MANY_REQUEST:
        return "Too Many Request";
      case INTERNAL_SERVER_ERROR:
        return "Internal Server Error";
      default:
        return "Unknown";
    }
  }

  @override
  String toString() => "{HttpStatus={code=$code,message=$message}}";
}

class CancellationException implements Exception {
  @pragma("vm:entry-point")
  const CancellationException();

  String toString() => "CancellationException";
}

abstract class NetworkResult<G, E> {
  NetworkResult._();

  factory NetworkResult.good(G value, HttpStatus httpStatus) => NetworkResultGood<G, E>(value, httpStatus);

  factory NetworkResult.badRequestData(Exception exception) => NetworkResultBadRequestData<G, E>(exception);

  factory NetworkResult.malformed(String value, HttpStatus httpStatus, Exception exception) => NetworkResultMalformed<G, E>(value, httpStatus, exception);

  factory NetworkResult.httpError(E value, HttpStatus httpStatus) => NetworkResultHttpError<G, E>(value, httpStatus);

  factory NetworkResult.httpErrorMalformed(String value, HttpStatus httpStatus, Exception exception) => NetworkResultHttpErrorMalformed<G, E>(value, httpStatus, exception);

  factory NetworkResult.ioError(Exception exception) => NetworkResultIoError<G, E>(exception);

  factory NetworkResult.timeout(Duration timeout) => NetworkResultTimeout<G, E>(timeout);

  factory NetworkResult.cancelled() => NetworkResultCancelled<G, E>();
}

class NetworkResultGood<G, _> extends NetworkResult<G, _> {
  final G value;
  final HttpStatus httpStatus;

  NetworkResultGood(this.value, this.httpStatus) : super._();

  @override
  String toString() => "{NetworkResultGood={value=$value,httpStatus=$httpStatus}}";
}

class NetworkResultBadRequestData<_, __> extends NetworkResult<_, __> {
  final Exception exception;

  NetworkResultBadRequestData(this.exception) : super._();

  @override
  String toString() => "{NetworkResultBadRequestData={exception=$exception}}";
}

class NetworkResultHttpError<_, E> extends NetworkResult<_, E> {
  final E value;
  final HttpStatus httpStatus;

  NetworkResultHttpError(this.value, this.httpStatus) : super._();

  @override
  String toString() => "{NetworkResultHttpError={value=$value,httpStatus=$httpStatus}}";
}

class NetworkResultMalformed<_, __> extends NetworkResult<_, __> {
  final String value;
  final HttpStatus httpStatus;
  final Exception exception;

  NetworkResultMalformed(this.value, this.httpStatus, this.exception) : super._();

  @override
  String toString() => "{NetworkResultMalformed={value=$value,httpStatus=$httpStatus,exception=$exception}}";
}

class NetworkResultHttpErrorMalformed<_, __> extends NetworkResult<_, __> {
  final String value;
  final HttpStatus httpStatus;
  final Exception exception;

  NetworkResultHttpErrorMalformed(this.value, this.httpStatus, this.exception) : super._();

  @override
  String toString() => "{NetworkResultHttpErrorMalformed={value=$value,httpStatus=$httpStatus,exception=$exception}}";
}

class NetworkResultIoError<_, __> extends NetworkResult<_, __> {
  final Exception exception;

  NetworkResultIoError(this.exception) : super._();

  @override
  String toString() => "{NetworkResultIoError={exception=$exception}";
}

class NetworkResultTimeout<_, __> extends NetworkResult<_, __> {
  final Duration timeout;

  NetworkResultTimeout(this.timeout) : super._();

  @override
  String toString() => "{NetworkResultTimeout={timeout=${timeout.toString()}}";
}

class NetworkResultCancelled<_, __> extends NetworkResult<_, __> {
  NetworkResultCancelled() : super._();

  @override
  String toString() => "{NetworkResultCancelled}";
}

typedef JsonDeserializer<T> = T Function(String string);
typedef JsonSerializer<T> = String Function(T data);

class JsonClient extends BaseClient {
  final Client _client = Client();

  Future<StreamedResponse> send(BaseRequest request) {
    request.headers['Content-type'] = "application/json";
    request.headers['Accept'] = "application/json,*/*";
    return _client.send(request);
  }
}

class NetworkHelper {
  final Map<String, Completer> _runningCompleterMap = Map();
  static final NetworkHelper _instance = NetworkHelper._internal();

  factory NetworkHelper() => _instance;

  NetworkHelper._internal();

  NetworkResult<G, E> _createNetworkResultFromResponse<G, E>(Response response,
      JsonDeserializer<G> jsonDeserializer,
      JsonDeserializer<E> errorJsonDeserializer,) {
    NetworkResult<G, E> networkResult;
    if (response.statusCode >= 200 && response.statusCode <= 299) {
      if (jsonDeserializer != null) {
        try {
          G object = jsonDeserializer(response.body);
          networkResult = NetworkResult.good(object, HttpStatus.fromResponse(response));
        } on Error catch (err) {
          networkResult = NetworkResult.malformed(response.body, HttpStatus.fromResponse(response), Exception(err.toString()));
        } catch (e) {
          networkResult = NetworkResult.malformed(response.body, HttpStatus.fromResponse(response), e);
        }
      } else {
        networkResult = NetworkResult.good(response.body as G, HttpStatus.fromResponse(response));
      }
    } else {
      if (errorJsonDeserializer != null) {
        try {
          E object = errorJsonDeserializer(response.body);
          networkResult = NetworkResult.httpError(object, HttpStatus.fromResponse(response));
        } on Error catch (err) {
          networkResult = NetworkResult.httpErrorMalformed(response.body, HttpStatus.fromResponse(response), Exception(err.toString()));
        } catch (e) {
          networkResult = NetworkResult.httpErrorMalformed(response.body, HttpStatus.fromResponse(response), e);
        }
      } else {
        networkResult = NetworkResult.httpError(response.body as E, HttpStatus.fromResponse(response));
      }
    }
    return networkResult;
  }

  Future<NetworkResult> _doAsync<P, G, E>(String method,
      String url,
      Map<String, String> headerMap,
      P data,
      JsonSerializer<P> jsonSerializer,
      JsonDeserializer<G> jsonDeserializer,
      JsonDeserializer<E> errorJsonDeserializer,
      Duration timeout,
      String username,
      String password,
      String token,
      String tag,) async {
    NetworkResult<G, E> networkResult;
    JsonClient jsonClient = JsonClient();
    Completer<Response> completer = Completer();
    if (tag != null) {
      _runningCompleterMap[tag] = completer;
    } else {
      _runningCompleterMap[url] = completer;
    }
    headerMap ??= Map();
    if (token != null) {
      headerMap["Authorization"] = "Bearer $token";
    } else if (username != null && password != null) {
      final credentials = "$username:$password";
      final encodedCredentials = utf8.fuse(base64).encode(credentials);
      headerMap["Authorization"] = "Basic $encodedCredentials";
    }
    try {
      Future<Response> futureResponse;
      switch (method) {
        case "GET":
          {
            futureResponse = jsonClient.get(url, headers: headerMap).timeout(timeout);
          }
          break;
        case "POST":
          {
            futureResponse = jsonClient.post(url, headers: headerMap, body: data != null ? jsonSerializer(data) : null).timeout(timeout);
          }
          break;
        case "PUT":
          {
            futureResponse = jsonClient.put(url, headers: headerMap, body: data != null ? jsonSerializer(data) : null).timeout(timeout);
          }
          break;
        case "PATCH":
          {
            futureResponse = jsonClient.patch(url, headers: headerMap, body: data != null ? jsonSerializer(data) : null).timeout(timeout);
          }
          break;
        case "DELETE":
          {
            futureResponse = jsonClient.delete(url, headers: headerMap).timeout(timeout);
          }
          break;
      }
      futureResponse.then((response) {
        if (!completer.isCompleted) {
          completer.complete(response);
        }
      }, onError: (error) {
        if (!completer.isCompleted) {
          completer.completeError(error);
        }
        return null;
      });
      Response response = await completer.future;
      networkResult = _createNetworkResultFromResponse(response, jsonDeserializer, errorJsonDeserializer);
    } on Error catch (err) {
      networkResult = NetworkResult.badRequestData(Exception(err.toString()));
    } on TimeoutException catch (e) {
      networkResult = NetworkResult.timeout(e.duration);
    } on CancellationException {
      networkResult = NetworkResult.cancelled();
    } catch (e) {
      networkResult = NetworkResult.ioError(e);
    } finally {
      if (tag != null) {
        _runningCompleterMap.remove(tag);
      } else {
        _runningCompleterMap.remove(url);
      }
      jsonClient.close();
    }
    debugUtil.log(networkResult.toString());
    return networkResult;
  }

  Future<NetworkResult> getAsync<G, E>(String url, {
    Map<String, String> headerMap,
    JsonDeserializer<G> jsonDeserializer,
    JsonDeserializer<E> errorJsonDeserializer,
    Duration timeout = const Duration(seconds: 10),
    String username,
    String password,
    String token,
    String tag,
  }) {
    return _doAsync(
      "GET",
      url,
      headerMap,
      null,
      null,
      jsonDeserializer,
      errorJsonDeserializer,
      timeout,
      username,
      password,
      token,
      tag,
    );
  }

  Future<NetworkResult> postAsync<P, G, E>(String url, {
    Map<String, String> headerMap,
    P data,
    JsonSerializer<P> jsonSerializer,
    JsonDeserializer<G> jsonDeserializer,
    JsonDeserializer<E> errorJsonDeserializer,
    Duration timeout = const Duration(seconds: 10),
    String username,
    String password,
    String token,
    String tag,
  }) {
    return _doAsync(
      "POST",
      url,
      headerMap,
      data,
      jsonSerializer,
      jsonDeserializer,
      errorJsonDeserializer,
      timeout,
      username,
      password,
      token,
      tag,
    );
  }

  Future<NetworkResult> putAsync<P, G, E>(String url, {
    Map<String, String> headerMap,
    P data,
    JsonSerializer<P> jsonSerializer,
    JsonDeserializer<G> jsonDeserializer,
    JsonDeserializer<E> errorJsonDeserializer,
    Duration timeout = const Duration(seconds: 10),
    String username,
    String password,
    String token,
    String tag,
  }) {
    return _doAsync(
      "PUT",
      url,
      headerMap,
      data,
      jsonSerializer,
      jsonDeserializer,
      errorJsonDeserializer,
      timeout,
      username,
      password,
      token,
      tag,
    );
  }

  Future<NetworkResult> patchAsync<P, G, E>(String url, {
    Map<String, String> headerMap,
    P data,
    JsonSerializer<P> jsonSerializer,
    JsonDeserializer<G> jsonDeserializer,
    JsonDeserializer<E> errorJsonDeserializer,
    Duration timeout = const Duration(seconds: 10),
    String username,
    String password,
    String token,
    String tag,
  }) {
    return _doAsync(
      "PATCH",
      url,
      headerMap,
      data,
      jsonSerializer,
      jsonDeserializer,
      errorJsonDeserializer,
      timeout,
      username,
      password,
      token,
      tag,
    );
  }

  Future<NetworkResult> deleteAsync<G, E>(String url, {
    Map<String, String> headerMap,
    JsonDeserializer<G> jsonDeserializer,
    JsonDeserializer<E> errorJsonDeserializer,
    Duration timeout = const Duration(seconds: 10),
    String username,
    String password,
    String token,
    String tag,
  }) {
    return _doAsync(
      "DELETE",
      url,
      headerMap,
      null,
      null,
      jsonDeserializer,
      errorJsonDeserializer,
      timeout,
      username,
      password,
      token,
      tag,
    );
  }

  void cancel(String tag) {
    Completer completer = _runningCompleterMap[tag];
    if (completer != null && !completer.isCompleted) {
      completer.completeError(CancellationException());
    }
  }
}

NetworkHelper networkHelper = NetworkHelper();
