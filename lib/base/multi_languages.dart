import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

class LocalizedStringResources {
  final Map<String, String> _values;

  LocalizedStringResources(this._values);

  static LocalizedStringResources of(BuildContext context) {
    return Localizations.of<LocalizedStringResources>(context, LocalizedStringResources);
  }

  String string(String resourceName) {
    return _values[resourceName];
  }
}

abstract class MultiLanguagesDelegate extends LocalizationsDelegate<LocalizedStringResources> {
  MultiLanguagesDelegate() {
    _fixMissingResources();
  }

  @override
  bool isSupported(Locale locale) => _findKeyForLocale(locale) != null;

  @override
  Future<LocalizedStringResources> load(Locale locale) => SynchronousFuture<LocalizedStringResources>(LocalizedStringResources(_stringResources(locale)));

  @override
  bool shouldReload(MultiLanguagesDelegate old) => false;

  Map<String, String> _stringResources(Locale locale) => localizedValues[_findKeyForLocale(locale)];

  Locale _findKeyForLocale(Locale locale) {
    Locale foundKey;
    final Iterable<Locale> keys = localizedValues.keys;

    foundKey = keys.firstWhere(
      (key) {
        return key == locale;
      },
      orElse: () => null,
    );

    if (foundKey == null) {
      final languageRegionTag = "${locale.languageCode}-${locale.countryCode}";
      foundKey = keys.firstWhere(
        (key) {
          return "${key.languageCode}-${key.countryCode}" == languageRegionTag;
        },
        orElse: () => null,
      );
    }

    if (foundKey == null) {
      final languageScriptTag = "${locale.languageCode}-${locale.scriptCode}";
      foundKey = keys.firstWhere(
        (key) {
          return "${key.languageCode}-${key.scriptCode}" == languageScriptTag;
        },
        orElse: () => null,
      );
    }

    if (foundKey == null) {
      final languageTag = locale.languageCode;
      foundKey = keys.firstWhere(
        (key) {
          return key.languageCode == languageTag;
        },
        orElse: () => null,
      );
    }

    return foundKey;
  }

  void _fixMissingResources() {
    final Iterable<Locale> keysSource = localizedValues.keys;
    final Iterable<Locale> keysTarget = localizedValues.keys;
    keysSource.forEach((keySource) {
      final Map<String, String> sourceResources = localizedValues[keySource];
      final Iterable<String> stringKeysSource = sourceResources.keys;
      keysTarget.forEach((keyTarget) {
        if (keyTarget != keySource) {
          final Map<String, String> targetResources = localizedValues[keyTarget];
          stringKeysSource.forEach((stringKeySource) {
            targetResources.putIfAbsent(stringKeySource, () {
              return sourceResources[stringKeySource];
            });
          });
        }
      });
    });
  }

  Iterable<Locale> get supportedLocales => localizedValues.keys;

  ///
  /// Example
  ///
  /// @override
  ///  Map<Locale, Map<String, String>> localizedValues = {
  /// Locale.fromSubtags(languageCode: 'en'): {
  /// 'appName': 'Flutter Base Application',
  /// },
  /// Locale.fromSubtags(languageCode: 'zh', scriptCode: 'Hans', countryCode: 'CN'): {
  /// 'appName': 'Flutter基础应用',
  /// },
  /// Locale.fromSubtags(languageCode: 'zh', countryCode: 'TW'): {
  /// 'appName': 'Flutter基礎應用',
  /// },
  /// };
  ///
  Map<Locale, Map<String, String>> localizedValues;
}

/// To use i18n method
/// 1. Run something like `flutter pub run intl_translation:extract_to_arb --output-dir=lib/localizations lib/localizations/app_multi_languages.dart` in the root directory
/// 2. Create copy of 'intl_messages.arb' as 'intl_en.arb', 'intl_zh_Hans_CN.arb' and 'intl_zh_TW.arb', etc.
/// 3. Run something like `flutter pub run intl_translation:generate_from_arb --output-dir=lib/localizations --no-use-deferred-loading lib/localizations/app_multi_languages.dart lib/localizations/intl_*.arb` in the root directory
abstract class LocalizedStringResources18n {
  final String locale;

  LocalizedStringResources18n(this.locale);

  static T of<T extends LocalizedStringResources18n>(BuildContext context) {
    return Localizations.of<T>(context, T);
  }

  ///
  /// In your sub classes add get-properties like below:
  ///
  /// String get appName {
  ///    return Intl.message(
  ///      'Flutter Base Application',
  ///      name: 'appName',
  ///      locale: locale,
  ///    );
  ///  }
}

abstract class MultiLanguages18nDelegate<T extends LocalizedStringResources18n> extends LocalizationsDelegate<T> {
  @override
  bool isSupported(Locale locale) => _findSuitableLocale(locale) != null;

  @override
  Future<T> load(Locale locale) {
    final String name = (locale.countryCode == null || locale.countryCode.isEmpty) ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);
    return createLocalizedStringResources18n(localeName);
  }

  @override
  bool shouldReload(MultiLanguages18nDelegate<T> old) => false;

  Locale _findSuitableLocale(Locale locale) {
    Locale foundSuitable;

    foundSuitable = supportedLocales.firstWhere(
      (key) {
        return key == locale;
      },
      orElse: () => null,
    );

    if (foundSuitable == null) {
      final languageRegionTag = "${locale.languageCode}-${locale.countryCode}";
      foundSuitable = supportedLocales.firstWhere(
        (key) {
          return "${key.languageCode}-${key.countryCode}" == languageRegionTag;
        },
        orElse: () => null,
      );
    }

    if (foundSuitable == null) {
      final languageScriptTag = "${locale.languageCode}-${locale.scriptCode}";
      foundSuitable = supportedLocales.firstWhere(
        (key) {
          return "${key.languageCode}-${key.scriptCode}" == languageScriptTag;
        },
        orElse: () => null,
      );
    }

    if (foundSuitable == null) {
      final languageTag = locale.languageCode;
      foundSuitable = supportedLocales.firstWhere(
        (key) {
          return key.languageCode == languageTag;
        },
        orElse: () => null,
      );
    }

    return foundSuitable;
  }

  ///
  /// Example
  ///
  /// @override
  ///  Future<AppLocalizedStringResources18n> createLocalizedStringResources18n(String localeName) {
  ///    return initializeMessages(localeName).then((_) {
  ///      return AppLocalizedStringResources18n(localeName);
  ///    });
  ///  }
  Future<T> createLocalizedStringResources18n(String localeName);

  ///
  /// Example
  ///
  /// @override
  ///  Iterable<Locale> get supportedLocales => [
  ///        Locale.fromSubtags(languageCode: 'en'),
  ///        Locale.fromSubtags(languageCode: 'zh', scriptCode: 'Hans', countryCode: 'CN'),
  ///        Locale.fromSubtags(languageCode: 'zh', countryCode: 'TW'),
  ///      ];
  Iterable<Locale> get supportedLocales;
}
