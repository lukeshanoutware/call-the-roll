import 'package:call_the_roll/model/teacher.dart';
import 'package:flutter/material.dart';

class ApplicationModel {
  static const Color WINDOW_BACKGROUND = Color(0XFFF5F5F7);
  static const Color COLOR_PRIMARY = Color(0XFF1A3FA2);
  static const Color COLOR_CAPTION = Color(0XFF92929D);
  static const Color BOTTOM_NAVIGATION_BAR_BACKGROUND = Color(0XFFF9F9F9);
  static const Color ACCENT_GREEN = Color(0XFF84C346);
  static const Color AVATAR_BACKGROUND = Color(0XFF323232);
  static const Color DIVIDER = Color(0XFFE2E2EA);
  static const Color SHADOW = Color(0X19CDCDD4);
  static const String API_URL = "https://us-central1-call-the-roll-945ca.cloudfunctions.net/callTheRoll/";
  static const String CLASS_ID = "DET007";
  static const Teacher TEACHER = Teacher("Tristan Sternson", "tristan.sternson@arq.group");
  static const bool ANIMATED_LIST = false;
  static final ApplicationModel _instance = ApplicationModel._internal();

  factory ApplicationModel() => _instance;

  ApplicationModel._internal();
}

ApplicationModel applicationModel = ApplicationModel();
