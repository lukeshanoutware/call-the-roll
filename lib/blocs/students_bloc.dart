import 'dart:async';
import 'dart:convert';

import 'package:call_the_roll/base/bloc.dart';
import 'package:call_the_roll/base/debug_util.dart';
import 'package:call_the_roll/base/network_helper.dart';
import 'package:call_the_roll/base/string_util.dart';
import 'package:call_the_roll/model/roll_calling.dart';
import 'package:call_the_roll/model/student.dart';
import 'package:call_the_roll/model/student_list_divider.dart';

import '../application_model.dart';

///-----------------------------------------------------------------------------
/// Events
///-----------------------------------------------------------------------------
abstract class StudentsEvent {}

class StudentsLoad extends StudentsEvent {}

class StudentsSwitchAttendance extends StudentsEvent {
  final Student student;

  StudentsSwitchAttendance(this.student);
}

class StudentsDoneClicked extends StudentsEvent {}

class StudentsNoClicked extends StudentsEvent {}

class StudentsYesClicked extends StudentsEvent {}

class StudentsSendClicked extends StudentsEvent {
  final String message;

  StudentsSendClicked(this.message);
}

class StudentsBackClicked extends StudentsEvent {}

///-----------------------------------------------------------------------------
/// States
///-----------------------------------------------------------------------------
abstract class StudentsState {
  final List<Student> studentList;
  final String counterString;
  final int absenceCount;

  StudentsState(this.studentList, this.counterString, this.absenceCount);
}

class StudentsInitial extends StudentsState {
  StudentsInitial() : super(null, null, null);
}

class StudentsLoading extends StudentsState {
  StudentsLoading() : super(null, null, null);
}

class StudentsLoaded extends StudentsState {
  StudentsLoaded(List<Student> studentList, String counterString, int absenceCount) : super(studentList, counterString, absenceCount);
}

class StudentsSwitchAttendanceDone extends StudentsState {
  final Student student;
  final int beforeIndex;
  final int afterIndex;

  StudentsSwitchAttendanceDone(List<Student> studentList, String counterString, int absenceCount, this.student, this.beforeIndex, this.afterIndex) : super(studentList, counterString, absenceCount);
}

class StudentsSending extends StudentsState {
  StudentsSending() : super(null, null, null);
}

class StudentsSendSuccess extends StudentsState {
  StudentsSendSuccess() : super(null, null, null);
}

class StudentsSendFailure extends StudentsState {
  final String message;

  StudentsSendFailure(this.message) : super(null, null, null);
}

class StudentsAskIfSend extends StudentsState {
  final String message;

  StudentsAskIfSend(this.message) : super(null, null, null);
}

class StudentsShowMessageTemplate extends StudentsState {
  final String message;

  StudentsShowMessageTemplate(this.message) : super(null, null, null);
}

class StudentsRollCallingDone extends StudentsState {
  StudentsRollCallingDone() : super(null, null, null);
}

class StudentsDummy extends StudentsState {
  StudentsDummy() : super(null, null, null);
}

///-----------------------------------------------------------------------------
/// BLoC
///-----------------------------------------------------------------------------
class StudentsBLoC extends BLoC<StudentsEvent, StudentsState> {
  static const String ROLL_CALLING_URL = "https://us-central1-call-the-roll-945ca.cloudfunctions.net/callTheRoll/";
  List<Student> _studentList;
  String _counter;
  int _absenceCount;

  @override
  get initialState => StudentsInitial();

  @override
  Stream<StudentsState> mapEventToStates(event) async* {
    if (event is StudentsLoad) {
      yield* _load();
    } else if (event is StudentsSwitchAttendance) {
      yield* _switchAttendance(event.student);
    } else if (event is StudentsDoneClicked) {
      yield* _onDoneClicked();
    } else if (event is StudentsNoClicked) {
      yield* _onNoClicked();
    } else if (event is StudentsYesClicked) {
      yield* _onYesClicked();
    } else if (event is StudentsSendClicked) {
      yield* _onSendClicked(event.message);
    } else if (event is StudentsBackClicked) {
      yield* _onBackClicked();
    }
  }

  @override
  Future<void> onTransaction(BLoCTransaction<StudentsEvent, StudentsState> transaction) async {
    debugUtil.log(transaction.toString());
  }

  Stream<StudentsState> _load() async* {
    debugUtil.mark();
    yield StudentsLoading();
    if (_studentList == null) {
      await Future.delayed(Duration(seconds: 1));
      await _generateStudentList();
      await _updateCounter();
      await _sortStudentList();
    }
    yield StudentsLoaded(_studentList, _counter, _absenceCount);
  }

  Stream<StudentsState> _switchAttendance(Student student) async* {
    debugUtil.mark();
    int beforeIndex = _studentList.indexOf(student);
    student.attendance = !student.attendance;
    await _updateCounter();
    await _sortStudentList();
    int afterIndex = _studentList.indexOf(student);
    yield StudentsSwitchAttendanceDone(_studentList, _counter, _absenceCount, student, beforeIndex, afterIndex);
  }

  Stream<StudentsState> _onDoneClicked() async* {
    debugUtil.mark();
    if (_absenceCount > 0) {
      String message = 'There ${_absenceCount > 1 ? "are" : "is"} $_absenceCount ${_absenceCount > 1 ? "students" : "student"} absent from your class. Would you like to send the parents a notification?';
      yield StudentsAskIfSend(message);
      yield StudentsDummy();
    } else {
      yield StudentsRollCallingDone();
      _studentList = null;
      yield StudentsInitial();
    }
  }

  Stream<StudentsState> _onNoClicked() async* {
    debugUtil.mark();
    yield StudentsRollCallingDone();
    _studentList = null;
    yield StudentsInitial();
  }

  Stream<StudentsState> _onYesClicked() async* {
    debugUtil.mark();
    yield StudentsShowMessageTemplate("This is a friendly message to inform you that your child is not in class today.");
  }

  Stream<StudentsState> _onSendClicked(String message) async* {
    debugUtil.mark();
    yield StudentsSending();
    RollCalling rollCalling = RollCalling(ApplicationModel.CLASS_ID, _studentList, ApplicationModel.TEACHER, message, DateTime.now());
    NetworkResult networkResult = await networkHelper.postAsync<RollCalling, String, String>(
      ROLL_CALLING_URL,
      data: rollCalling,
      jsonSerializer: (rollCalling) {
        return jsonEncode(rollCalling.toMap());
      },
      jsonDeserializer: (string) {
        return string;
      },
      errorJsonDeserializer: (string) {
        return string;
      },
    );
    if (networkResult is NetworkResultGood) {
      yield StudentsSendSuccess();
      yield StudentsRollCallingDone();
      _studentList = null;
      yield StudentsInitial();
    } else {
      yield StudentsSendFailure(networkResult.toString());
    }
  }

  Stream<StudentsState> _onBackClicked() async* {
    debugUtil.mark();
    yield StudentsLoaded(_studentList, _counter, _absenceCount);
  }

  Future<void> _sortStudentList() async {
    _studentList.sort((a, b) {
      if (a.name != null && b.name != null) {
        if (a.attendance == b.attendance) {
          return a.name.compareTo(b.name);
        } else {
          return a.attendance ? 1 : -1;
        }
      } else {
        if (a.name == null) {
          return b.attendance ? -1 : 1;
        } else {
          return a.attendance ? 1 : -1;
        }
      }
    });
  }

  Future<void> _updateCounter() async {
    int totalCount = _studentList.length - 1;
    int attendanceCount = _studentList
        .where((student) => student.attendance != null && student.attendance)
        .length;
    _absenceCount = totalCount - attendanceCount;
    _counter = "$attendanceCount/$totalCount";
  }

  Future<void> _generateStudentList() async {
    _studentList = [
      Student("Abrar Peer", false, null, "", null),
      Student("Alexander Manoharan", false, "assets/images/ryu.png", "", null),
      Student("Cassie Milford", false, null, "", null),
      Student("Cynthia Fu", false, "assets/images/chunli.png", "", null),
      Student("Hannah Pearce", false, null, "", null),
      Student("Jay Christie", false, "assets/images/zangifu.png", "", null),
      Student("Keith Barton", false, null, "", null),
      Student("Luke Shan", false, null, "", null),
      Student("Roman Maya", false, null, "", null),
      Student("Timothy Kuusik", false, null, "", null),
      StudentListDivider(),
    ];
    _studentList.forEach((student) {
      student.initials = getInitials(student.name);
      student.color = getColor(student.name, 0.4, 0.7);
    });
  }

  void load() {
    addEvent(StudentsLoad());
  }

  void switchAttendance(Student student) {
    addEvent(StudentsSwitchAttendance(student));
  }

  void onDoneClicked() {
    addEvent(StudentsDoneClicked());
  }

  void onNoClicked() {
    addEvent(StudentsNoClicked());
  }

  void onYesClicked() {
    addEvent(StudentsYesClicked());
  }

  void onSendClicked(String message) {
    addEvent(StudentsSendClicked(message));
  }

  void onBackClicked() {
    addEvent(StudentsBackClicked());
  }
}
