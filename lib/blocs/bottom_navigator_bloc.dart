import 'package:call_the_roll/base/bloc.dart';
import 'package:call_the_roll/base/debug_util.dart';
import 'package:call_the_roll/pages/documents.dart';
import 'package:call_the_roll/pages/notes.dart';
import 'package:call_the_roll/pages/overview.dart';
import 'package:call_the_roll/pages/schedule.dart';
import 'package:call_the_roll/pages/students.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

///-----------------------------------------------------------------------------
/// Events
///-----------------------------------------------------------------------------
abstract class BottomNavigatorEvent {}

class BottomNavigatorToIndex extends BottomNavigatorEvent {
  final int index;

  BottomNavigatorToIndex(this.index);
}

///-----------------------------------------------------------------------------
/// States
///-----------------------------------------------------------------------------
abstract class BottomNavigatorState {
  final int index;
  final String title;
  final Widget icon;
  final Widget route;

  BottomNavigatorState(this.index, this.title, this.icon, this.route);
}

class BottomNavigatorToOverview extends BottomNavigatorState {
  static final BottomNavigatorToOverview _instance = BottomNavigatorToOverview._internal();

  factory BottomNavigatorToOverview() => _instance;

  BottomNavigatorToOverview._internal() : super(0, "Overview", SvgPicture.asset("assets/images/ic_dashboard.svg"), OverviewRoute());
}

BottomNavigatorToOverview bottomNavigatorToOverview = BottomNavigatorToOverview();

class BottomNavigatorToStudents extends BottomNavigatorState {
  static final BottomNavigatorToStudents _instance = BottomNavigatorToStudents._internal();

  factory BottomNavigatorToStudents() => _instance;

  BottomNavigatorToStudents._internal() : super(1, "Students", SvgPicture.asset("assets/images/ic_student_outline.svg"), StudentsRoute());
}

BottomNavigatorToStudents bottomNavigatorToStudents = BottomNavigatorToStudents();

class BottomNavigatorToSchedule extends BottomNavigatorState {
  static final BottomNavigatorToSchedule _instance = BottomNavigatorToSchedule._internal();

  factory BottomNavigatorToSchedule() => _instance;

  BottomNavigatorToSchedule._internal() : super(2, "Schedule", SvgPicture.asset("assets/images/ic_schedule.svg"), ScheduleRoute());
}

BottomNavigatorToSchedule bottomNavigatorToSchedule = BottomNavigatorToSchedule();

class BottomNavigatorToDocuments extends BottomNavigatorState {
  static final BottomNavigatorToDocuments _instance = BottomNavigatorToDocuments._internal();

  factory BottomNavigatorToDocuments() => _instance;

  BottomNavigatorToDocuments._internal() : super(3, "Documents", SvgPicture.asset("assets/images/ic_attachment.svg"), DocumentsRoute());
}

BottomNavigatorToDocuments bottomNavigatorToDocuments = BottomNavigatorToDocuments();

class BottomNavigatorToNotes extends BottomNavigatorState {
  static final BottomNavigatorToNotes _instance = BottomNavigatorToNotes._internal();

  factory BottomNavigatorToNotes() => _instance;

  BottomNavigatorToNotes._internal() : super(4, "Notes", SvgPicture.asset("assets/images/ic_write_message.svg"), NotesRoute());
}

BottomNavigatorToNotes bottomNavigatorToNotes = BottomNavigatorToNotes();

///-----------------------------------------------------------------------------
/// BLoC
///-----------------------------------------------------------------------------
class BottomNavigatorBLoC extends BLoC<BottomNavigatorEvent, BottomNavigatorState> {
  @override
  get initialState => bottomNavigatorToStudents;

  @override
  Stream<BottomNavigatorState> mapEventToStates(event) async* {
    if (event is BottomNavigatorToIndex) {
      yield* _navigateToIndex(event.index);
    }
  }

  @override
  Future<void> onTransaction(BLoCTransaction<BottomNavigatorEvent, BottomNavigatorState> transaction) async {
    debugUtil.log(transaction.toString());
  }

  Stream<BottomNavigatorState> _navigateToIndex(int index) async* {
    debugUtil.mark();
    switch (index) {
      case 0:
        yield bottomNavigatorToOverview;
        break;
      case 1:
        yield bottomNavigatorToStudents;
        break;
      case 2:
        yield bottomNavigatorToSchedule;
        break;
      case 3:
        yield bottomNavigatorToDocuments;
        break;
      case 4:
        yield bottomNavigatorToNotes;
        break;
      default:
        yield bottomNavigatorToOverview;
        break;
    }
  }

  void navigateToIndex(int index) {
    //This line is commented out because we don't want user to navigate to other tabs.
    //addEvent(BottomNavigatorToIndex(index));
  }

  List<BottomNavigatorState> getAllItems() {
    return [
      bottomNavigatorToOverview,
      bottomNavigatorToStudents,
      bottomNavigatorToSchedule,
      bottomNavigatorToDocuments,
      bottomNavigatorToNotes,
    ];
  }
}
