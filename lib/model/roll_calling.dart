import 'package:call_the_roll/model/student.dart';
import 'package:call_the_roll/model/teacher.dart';
import 'package:json_annotation/json_annotation.dart';

part 'roll_calling.g.dart';

@JsonSerializable(explicitToJson: true, createFactory: false)
class RollCalling {
  @JsonKey(name: 'class_id')
  String classId;
  @JsonKey(name: 'student_list')
  List<Student> studentList;
  @JsonKey(name: 'teacher')
  Teacher teacher;
  @JsonKey(name: 'message')
  String message;
  @JsonKey(name: 'update_time')
  DateTime updateTime;

  RollCalling(this.classId, this.studentList, this.teacher, this.message, this.updateTime);

  Map<String, dynamic> toMap() => _$RollCallingToJson(this);
}
