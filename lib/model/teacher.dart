import 'package:json_annotation/json_annotation.dart';

part 'teacher.g.dart';

@JsonSerializable()
class Teacher {
  @JsonKey(name: 'name')
  final String name;
  @JsonKey(name: 'email')
  final String email;

  const Teacher(this.name, this.email);

  factory Teacher.fromJson(Map<String, dynamic> json) => _$TeacherFromJson(json);

  Map<String, dynamic> toJson() => _$TeacherToJson(this);
}
