// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'roll_calling.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Map<String, dynamic> _$RollCallingToJson(RollCalling instance) =>
    <String, dynamic>{
      'class_id': instance.classId,
      'student_list': instance.studentList?.map((e) => e?.toJson())?.toList(),
      'teacher': instance.teacher?.toJson(),
      'message': instance.message,
      'update_time': instance.updateTime?.toIso8601String(),
    };
