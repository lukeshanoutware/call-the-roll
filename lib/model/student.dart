import 'dart:ui';

import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'student.g.dart';

@JsonSerializable(explicitToJson: true, createFactory: false)
class Student extends Equatable {
  @JsonKey(name: 'name')
  String name;
  @JsonKey(name: 'attendance')
  bool attendance;
  @JsonKey(name: 'avatar')
  String avatar;
  @JsonKey(name: 'initials')
  String initials;
  @JsonKey(name: 'color', ignore: true)
  Color color;

  Student(this.name, this.attendance, this.avatar, this.initials, this.color);

  Map<String, dynamic> toJson() => _$StudentToJson(this);

  @override
  List<Object> get props => [name];
}
