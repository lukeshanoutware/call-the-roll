// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'student.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Map<String, dynamic> _$StudentToJson(Student instance) => <String, dynamic>{
  'stringify': instance.stringify,
  'hashCode': instance.hashCode,
      'name': instance.name,
      'attendance': instance.attendance,
  'avatar': instance.avatar,
  'initials': instance.initials,
  'props': instance.props,
    };
