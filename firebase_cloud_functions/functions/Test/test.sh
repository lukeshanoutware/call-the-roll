firebaseHostingURL="https://us-central1-call-the-roll-945ca.cloudfunctions.net/callTheRoll"

# Testing create 
curl -d '{"class_id":"DET007","student_list":[{"name":"Abrar Peer","attendance":true},{"name":"Alexander Manoharan","attendance":true},{"name":"Cassie Milford","attendance":true},{"name":"Cynthia Fu","attendance":false},{"name":"Dipanjan Dutta","attendance":true},{"name":"Hanani","attendance":true},{"name":"Hannah Pearce","attendance":true},{"name":"Jared Linwood","attendance":true},{"name":"Jay Christie","attendance":true},{"name":"Keith Barton","attendance":true},{"name":"Kishor Sharma","attendance":true},{"name":"Luke Shan","attendance":true},{"name":"Lyell Lamborn","attendance":true},{"name":"Paul Williams","attendance":true},{"name":"Roman Maya","attendance":true},{"name":"Sam Sibley","attendance":true},{"name":"Timothy Kuusik","attendance":true}],"teacher":{"name":"Tristan Sternson","email":"tristan.sternson@arq.group"},"update_time":"2020-01-15T10:03:53.875451"}' -H "Content-Type: application/json" -X POST "$firebaseHostingURL/"

curl -d '{"class_id":"DET007","student_list":[{"name":"Abrar Peer","attendance":true},{"name":"Alexander Manoharan","attendance":true},{"name":"Cassie Milford","attendance":true},{"name":"Cynthia Fu","attendance":false},{"name":"Dipanjan Dutta","attendance":false},{"name":"Hanani","attendance":true},{"name":"Hannah Pearce","attendance":true},{"name":"Jared Linwood","attendance":true},{"name":"Jay Christie","attendance":true},{"name":"Keith Barton","attendance":true},{"name":"Kishor Sharma","attendance":true},{"name":"Luke Shan","attendance":false},{"name":"Roman Maya","attendance":true},{"name":"Sam Sibley","attendance":true},{"name":"Timothy Kuusik","attendance":true}],"teacher":{"name":"Tristan Sternson","email":"tristan.sternson@arq.group"},"update_time":"2020-01-15T10:03:53.875451"}' -H "Content-Type: application/json" -X POST "$firebaseHostingURL/"


curl -G "$firebaseHostingURL/students/"

curl -G "$firebaseHostingURL/test/"

curl -G "https://us-central1-call-the-roll-945ca.cloudfunctions.net/callTheRollAPI/api/v1/sendMessage"


DATA='{"notification": {"body": "this is a body","title": "this is a title"}, "priority": "high", "data": {"click_action": "FLUTTER_NOTIFICATION_CLICK", "id": "1", "status": "done"}, "to": "<FCM TOKEN>"}'
curl https://fcm.googleapis.com/fcm/send -H "Content-Type:application/json" -X POST -d "$DATA" -H "Authorization: key=<FCM SERVER KEY>"


curl -X POST -H "Authorization: Bearer ya29.ElqKBGN2Ri_Uz...HnS_uNreA" -H "Content-Type: application/json" -d '{
"message":{
   "notification":{
     "title":"FCM Message",
     "body":"This is an FCM Message"
   },
   "token":"dWE1uca7Vk68usl7LkOGPo:APA91bEZnVyK6I8BADZWPGEpcIrIzvWSsihyiJqU5HvD6A9L7bytMJSHMFQc8SLxuI1cyepBg3LoZbjIMQSfpCtkvXk9NKRvlB45sDU64wkbhbjqBptQw5IAhLTFmO2CTMw1bW5Fs0ee"
}}' https://fcm.googleapis.com/v1/projects/myproject-b5ae1/messages:send


curl -G "$firebaseHostingURL/sendMessage/dWE1uca7Vk68usl7LkOGPo:APA91bEZnVyK6I8BADZWPGEpcIrIzvWSsihyiJqU5HvD6A9L7bytMJSHMFQc8SLxuI1cyepBg3LoZbjIMQSfpCtkvXk9NKRvlB45sDU64wkbhbjqBptQw5IAhLTFmO2CTMw1bW5Fs0ee/"


curl -G "$firebaseHostingURL/sendMessage/cVgWWA9YwoA:APA91bFRH92mM-dyYi_JqZg2t6qkvXkuseErXMUoewKiTvUh3dpd72hBTSyCrU3r7UJxiTo_MRBdpAlbsVKCtKnnpW2AHioAUAN5nlVtb6wUYcrMl3uFDHyrZ74JcgkhFJxhhzdHtyh5/"