import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as express from 'express';
import * as bodyParser from "body-parser";
import { DocumentData } from '@google-cloud/firestore';

// export const sendMessage = functions.https.onRequest((request, response) => {
//     response.send(sendFCM(request.body));
// });

// function sendFCM(requestBody: string) {
//     return `I want to send message ${requestBody}`
// }

admin.initializeApp(functions.config().firebase);
const db = admin.firestore(); // Add this

const app = express();
const main = express();
main.use('', app);
main.use(bodyParser.json());

export const callTheRoll = functions.https.onRequest(main);

export const helloWorld = functions.https.onRequest((request, response) => {
    response.send(`Hello from Firebase! ${request.body}`);
});

export interface Student {
    name: string,
    attendance: Boolean
}

export interface Teacher {
    name: string,
    email: String
}

export interface RollCalling { 
    class_id: string, 
    student_list: Student[], 
    teacher: Teacher,
    update_time: string 
}

export interface Parent { 
    device_id: string, 
    device_type: string,
    name: string,
    push_token: string,
    student_name: string
}

export interface CallTheRollResult { 
    student: Student, 
    parents: Parent[],
    sendMessageResult: string[]
}

// POST
app.post('/', async (request, response) => {
    try {
      const rollCalling = request.body as RollCalling;
      
      // Get all student whose the attendance is false
      const absentStudents = getAbsentStudent(rollCalling.student_list)

      const results: CallTheRollResult[] = []
      absentStudents.forEach( async (element) => {
        // For each absent student, search for it's parents, then send message per each
        const parents = await getParentsOfStudent(element)
        const messageResults = await sendMessageToParents(parents)
        //sendMessageToParents(parents)
        results.push({
            student: element, 
            parents: parents,
            sendMessageResult: messageResults
        });
      })
        response.json({
            id: results.length,
            data: results
        });
    } catch(error){
      response.status(500).send(error);
    }
  });

  app.get('/students', async (request, response) => {
    try {
        
      const snapshot = await db.collection('students').get();
      snapshot.forEach(
          (doc) => {
            response.json({
                      id: doc.id,
                      data: doc.data()
                  });
          }
      );
    } catch(error){
  
      response.status(500).send(error);
  
    }
  
  });

  app.get('/test', async (request, response) => {
    try {
        
        response.json({
            data: "Hello!!!!"
        })
    } catch(error){
      response.status(500).send(error);
    }
  });

  app.get('/sendMessage/:token', async (request, response) => {
    try {
        const registrationToken = request.params.token;
        // This registration token comes from the client FCM SDKs.
        //var registrationToken = 'token';

        const message = {
            notification: {
                title: 'test title',
                body: 'test body'
            },
            data: {
                title: 'test title',
                body: 'test body',
                click_action: "FLUTTER_NOTIFICATION_CLICK"
            },
            token: registrationToken
        };

        // Send a message to the device corresponding to the provided
        // registration token.
        const result = await admin.messaging().send(message)
        // .then((response) => {
        //     // Response is a message ID string.
        //     console.log('Successfully sent message:', response);
            
        //     response.json({
        //         token: registrationToken,
        //         result: "SUCCESS"
        //     })
        // })
        // .catch((error) => {
        //     console.log('Error sending message:', error);
        //     response.json({
        //         token: registrationToken,
        //         result: "ERROR"
        //     })
        // });
        response.json({
            token: registrationToken,
            result: result
        })
    } catch(error){
      response.status(500).send(error);
    }
  });

   
function getAbsentStudent(studentList: Student[]) {
    return studentList.filter((element, index, array) => { 
        return element.attendance === false; 
    })
}

async function getParentsOfStudent(student: Student) {
    const parentsQuerySnapshot = await db.collection('students').doc(student.name.toLowerCase()).collection('parents').get();
    const parents: Parent[] = [];
    parentsQuerySnapshot.forEach(
        (doc) => {
            if(doc.exists){
                let data: DocumentData = doc.data()
                parents.push({
                    device_id: data['device_id'],
                    device_type: data['device_type'],
                    name: data['name'],
                    push_token: data['push_token'],
                    student_name: data['student_name']
                });
            }
        }
    );
    return parents
}

function sendMessageToParents(parents: Parent[]) {
    const results: string[] = []
    parents.forEach( async (element) => {
        const result = await sendMessageToParent(element)
        results.push(result);
    }) 
    return results
}

async function sendMessageToParent(parent: Parent) {
    var registrationToken = parent.push_token;

    var message = {
        notification: {
            title: `Message from school`,
            body: `${parent.student_name} is absent. Please confirm!`,
        },
        data: {
            title: `Message from school`,
            body: `${parent.student_name} is absent. Please confirm!`,
            device_id: parent.device_id, 
            student_name: parent.student_name, 
            name: parent.name, 
            device_type: parent.device_type, 
            push_token: parent.push_token,
            click_action: "FLUTTER_NOTIFICATION_CLICK"
        },
        token: registrationToken
    };
    // const log = 'device_id:'+ parent.device_id 
    // + ', student_name: ' + parent.student_name
    // + ', name: '+ parent.name 
    // + ', device_type: '+ parent.device_type
    // + ', push_token: '+ parent.push_token
    console.log(`Sending message: ${message}...`);

    var result = ""
    await admin.messaging().send(message)
    .then((response) => {
        // Response is a message ID string.
        console.log('Successfully sent message:', response);
        result = `Successfully sent message: ${response}`
    })
    .catch((error) => {
        console.log('Error sending message:', error);
        result = `Error sending message: ${error}`
    });
    return result
}